#!/usr/bin/env bash

# Function definition
success_ () {
  echo -e "${GREEN} :Success${NOCOL}"
}

failure_ () {
  echo -e "${RED} :Failure${NOCOL}"
  exit 1
}

# Set some variables
BLUE='\033[0;36m'
GREEN='\033[0;32m'
YELLOW='\033[0;33m'
RED='\033[0;31m'
NOCOL='\033[0m'

# Get system data
GIT_VERSION=$(git --version || (echo "git error" && exit 1))
echo GIT_VERSION=${GIT_VERSION}

# Do git version comparison as hooks can only be --global after 2.9.0
GIT_VER_REGEX='([0-9]+)\.([0-9]+)\.([0-9]+).*$'
[[ ${GIT_VERSION} =~ ${GIT_VER_REGEX} ]]

if [[ "${BASH_REMATCH[1]}" -lt "2" ]]; then
  echo "Git version of ${BASH_REMATCH[0]} less than 2.9.0 mean hooks go into repo/.git/hooks rather than --global"
  global=''
else
  if [[ "${BASH_REMATCH[2]}" -lt "9" ]]; then
    echo "Git version of ${BASH_REMATCH[0]} less than 2.9.0 mean hooks go into repo/.git/hooks rather than --global"
    global=''
  fi
  global='true'
fi

# Get gpg keyid
echo -e "${BLUE}Enter your gpg key-id. If you don't want to sign your commits, hit enter to leave the keyid empty${NOCOL}"
read -p "Enter you gpg key-id: " gpg_keyid

# Confirm user data
echo -e "${BLUE}If initial setup, configuring git username and email address${NOCOL}"
GIT_USER=$(git config --get user.name)
if [[ ${GIT_USER} == ${USER} ]]; then
  echo -e "${YELLOW}Username ${USER} configured already${NOCOL}"
else
  echo -ne "\tgit config --global user.name ${USER}"
  git config --global user.name ${USER} && success_ || failure_
fi

GIT_EMAIL=$(git config --get user.email)
if [[ ${GIT_EMAIL} =~ '@' ]]; then
  echo -e "${YELLOW}Email with address ${GIT_EMAIL} already configured, not changing${NOCOL}"
else
  read -p "Enter your organisation email address: " EMAIL
  echo -ne "\tgit config --global user.email ${EMAIL}"
  git config --global user.email "${EMAIL}" && success_ || failure_
fi

# Some git config options
echo -e "${BLUE}Setting up some git options${NOCOL}"
echo -ne "\tgit config --global push.default current"
git config --global push.default current && success_ || failure_
echo -ne "\tgit config --global color.ui true"
git config --global color.ui true && success_ || failure_
echo -ne "\tgit config --global credential.helper cache"
git config --global credential.helper cache && success_ || failure_
echo -ne "\tgit config --global pager.branch false"
git config --global pager.branch false && success_ || failure_
echo -ne "\tgit config --global status.submoduleSummary true"
git config --global status.submoduleSummary true && success_ || failure_

if [[ ${gpg_keyid} != "" ]]; then
# Configure gpg commit signing
  echo -e "${BLUE}Setting up gpg commit signing${NOCOL}"
  echo -ne "\tgit config --global user.signingkey gpg_keyid"
  git config --global user.signingkey ${gpg_keyid} && success_ || failure_
  echo -ne "\tgit config --global commit.gpgsign true"
  git config --global commit.gpgsign true && success_ || failure_
fi

# Configure colour highlighting
echo -e "${BLUE}Setting up some git clour options${NOCOL}"
echo -ne "\tgit config --global color.ui auto"
git config --global color.ui true && success_ || failure_
echo -ne "\tgit config --global color.branch auto"
git config --global color.branch auto && success_ || failure_
echo -ne "\tgit config --global color.diff auto"
git config --global color.diff auto && success_ || failure_
echo -ne "\tgit config --global color.grep auto"
git config --global color.grep auto && success_ || failure_
echo -ne "\tgit config --global color.showbranch auto"
git config --global color.showbranch auto && success_ || failure_
echo -ne "\tgit config --global color.status auto"
git config --global color.status auto && success_ || failure_

# Configure aliases
echo -e "${BLUE}Setting up some git aliases${NOCOL}"
echo -ne "\tgit config --global alias.co checkout"
git config --global alias.co git checkout && success_ || failure_
echo -ne "\tgit config --global alias.br branch"
git config --global alias.br branch && success_ || failure_
echo -ne "\tgit config --global alias.ci commit"
git config --global alias.ct commit && success_ || failure_
echo -ne "\tgit config --global alias.st status"
git config --global alias.st status && success_ || failure_
echo -ne "\tgit config --global alias.unstage 'reset HEAD --'"
git config --global alias.unstage 'reset HEAD --' && success_ || failure_
echo -ne "\tgit config --global alias.last 'log -1 HEAD'"
git config --global alias.last 'log -1 HEAD' && success_ || failure_

# Configure editor prefences
echo -e "${BLUE}Setting up preference${NOCOL}"
echo -ne "\tSettin vim as default editor" && success_ || failure_
git config --global core.editor "vim"
echo -ne "\tgit config --global alias.co checkout"
git config --global alias.co checkout && success_ || failure_
echo -ne "\tInstalling global gitignore file"
cp -r gitignore_global ~/.gitignore_global && success_ || failure_
echo -ne "\tConfiguring global gitignore file"
git config --global core.excludesfile ~/.gitignore_global && success_ || failure_
echo -ne "\tConfiguring diff tool"
git config --global merge.tool vimdiff && success_ || failure_
echo -ne "\tConfiguring line ending settings for Linux and Mac, set to 'true' for Win"
git config --global core.autocrlf input && success_ || failure_
echo -ne "\tConfiguring merge conflict style"
git config --global merge.conflictstyle diff3 && success_ || failure_
echo -ne "\tConfiguring diff prompt to false"
git config --global difftool.prompt false && success_ || failure_

# Install commit message template
echo -e "${BLUE}Setting up commit message template into ~/.git/${NOCOL}"
echo -ne "\tEnsuring ~/.git directory is present"
if [[ ! -d ~/.git ]]; then
  mkdir ~/.git && success_ || failure_
elif [[ -d ~/.git ]]; then
  touch ~/.git && success_ || failure_ 
fi
echo -ne "\tInstalling commitmessage.txt to ~/.git"
cp commitmessage.txt ~/.git && success_ || failure_
echo -ne "\tgit config --global commit.template '~/.git/commitmessage.txt'"
git config --global commit.template "~/.git/commitmessage.txt"  && success_ || failure_

# Install hooks
echo -e "${BLUE}Setting up git hooks for git version ${GIT_VERSION}${NOCOL}"
if [[ ${global} == 'true' ]]; then
  echo -ne "\tgit config --global core.hookspath '~/.git/hooks'"
  git config --global core.hookspath "~/.git/hooks" && success_ || failure_

  echo -ne "\tInstalling hooks to ~/.git/hooks"
  cp -r hooks ~/.git && success_ || failure_

  echo -ne "\tSetting hook permissions"
  chmod +x ~/.git/hooks/* && success_ || failure_
else
  echo -ne "\tCopying hooks to ~/githooks"
  cp -r hooks ~/githooks && success_ || failure_

  echo -ne "\tSetting hook permissions"
  chmod +x ~/githooks/* && success_ || failure_
  echo -e "${YELLOW}The hooks in ~/githooks/ are for reference and must be copied to your --local git repo${NOCOL}"
  echo -e "${YELLOW}Consider upgrading your git version to > 2.9.0 for --global hook installation${NOCOL}"
fi

echo global=${global}
